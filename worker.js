function worker(gen, ...args) {
  const iterator = gen(...args);
  let resolve = null;

  function recurse(object) {
    if (object.done) {
      return resolve(object.value);
    }

    if (typeof object.value === 'function') {
      try {
        recurse(iterator.next(object.value()));
      } catch (error) {
        recurse(iterator.throw(object.value));
      }
      return;
    }

    if (object.value instanceof Promise) {
      object.value.then(val => recurse(iterator.next(val)));
      return;
    }

    recurse(iterator.next(object.value));
  }

  recurse(iterator.next());

  return new Promise(res => resolve = res);
}
